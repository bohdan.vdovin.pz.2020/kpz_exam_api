﻿using TeacherJournal.Models;
namespace TeacherJournal.Dto
{
    public class StudentDto
    {
        public StudentDto() { }
        public StudentDto(Student st)
        {
            Id = st.Id;
            FirstName = st.FirstName;
            LastName = st.LastName;
            Email = st.Email;
            Age = st.Age;
            Phone = st.Phone;
        }
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public int Age { get; set; }
        public string? Phone { get; set; }
    }
}
