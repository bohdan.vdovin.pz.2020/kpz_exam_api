﻿using TeacherJournal.Models;
namespace TeacherJournal.Dto
{
    public class TaskDto
    {
        public TaskDto()
        {
        }
        public TaskDto(Models.Task task)
        {
            Id = task.Id;
            Name = task.Name;
            MaxMark = task.MaxMark;
            Description = task.Description;
            Priority = task.Priority;
            Created = task.Created;
            StartAccept = task.StartAccept;
            CloseAccept = task.CloseAccept;
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public float MaxMark { get; set; }
        public string Description { get; set; }
        public string Priority { get; set; }
        public DateTime Created { get; set; }
        public DateTime StartAccept { get; set; }
        public DateTime CloseAccept { get; set; }
    }
}
