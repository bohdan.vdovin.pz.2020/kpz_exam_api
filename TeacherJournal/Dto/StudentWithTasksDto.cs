﻿namespace TeacherJournal.Dto
{
    public class StudentWithTasksDto
    {
        public int StudentId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }    
        public IEnumerable<StudentTaskDto> Marks { get; set; }

    }
}
