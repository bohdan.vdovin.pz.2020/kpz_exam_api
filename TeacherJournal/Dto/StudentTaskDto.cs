﻿using TeacherJournal.Models;
namespace TeacherJournal.Dto
{
    public class StudentTaskDto
    {
        public StudentTaskDto()
        {
        }
        public StudentTaskDto(StudentTask st)
        {
            TaskId = st.TaskId;
            StudentId = st.StudentId;
            Mark = st.Mark;
            Finished = st.Finished;
        }
        public int TaskId { get; set; }
        public int StudentId { get; set; }
        public float Mark { get; set; }
        public DateTime Finished { get; set; }
    }
}
