﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TeacherJournal.Interfaces;
using TeacherJournal.Dto;
using TeacherJournal.Models;

namespace TeacherJournal.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        protected IStudentRepository _studentRepository;
        public StudentController(IStudentRepository studentRepository)
        {
            _studentRepository = studentRepository;
        }

        [HttpGet]
        public ActionResult<IEnumerable<StudentDto>> GetStudents()
        {
            return _studentRepository.GetStudents().Select(s => new StudentDto(s)).ToList();
        }
        [HttpGet("{id}")]
        public ActionResult<StudentDto> GetStudent(int id)
        {
            return new StudentDto(_studentRepository.GetStudent(id));
        }

        [HttpPost]
        public ActionResult CreateStudent([FromBody] StudentDto studentToCreate)
        {
            if (studentToCreate == null)
                return BadRequest(ModelState);
            if (_studentRepository.CreateStudent(new Student
            {
                Id = studentToCreate.Id,
                FirstName = studentToCreate.FirstName,
                LastName = studentToCreate.LastName,
                Email = studentToCreate.Email,
                Age = studentToCreate.Age,
                Phone = studentToCreate.Phone
            }))
                return Ok();
            else
                return BadRequest();
        }
        [HttpPut]
        public ActionResult UpdateStudent([FromBody] StudentDto studentToUpdate)
        {
            if (studentToUpdate == null)
                return BadRequest();
            if (_studentRepository.UpdateStudent(new Student
            {
                Id = studentToUpdate.Id,
                FirstName = studentToUpdate.FirstName,
                LastName = studentToUpdate.LastName,
                Email = studentToUpdate.Email,
                Age = studentToUpdate.Age,
                Phone = studentToUpdate.Phone
            }))
                return Ok();
            else
                return BadRequest();
        }

        [HttpDelete("{id}")]
        public ActionResult DeleteStudent(int id)
        {
            if (_studentRepository.DeleteStudent(id))
                return Ok();
            else 
                return BadRequest();
        }
    }
}
