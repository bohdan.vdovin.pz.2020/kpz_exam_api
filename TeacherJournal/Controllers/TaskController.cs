﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TeacherJournal.Interfaces;
using TeacherJournal.Dto;
using TeacherJournal.Models;

namespace TeacherJournal.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        protected ITaskRepository _taskRepository;
        public TaskController(ITaskRepository taskRepository)
        {
            _taskRepository = taskRepository;
        }

        [HttpGet]
        public ActionResult<IEnumerable<TaskDto>> GetTasks()
        {
            return _taskRepository.GetTasks().Select(s => new TaskDto(s)).ToList();
        }
        [HttpGet("{id}")]
        public ActionResult<TaskDto> GetTask(int id)
        {
            return new TaskDto(_taskRepository.GetTask(id));
        }

        [HttpPost]
        public ActionResult CreateTask([FromBody] TaskDto taskToCreate)
        {
            if (taskToCreate == null)
                return BadRequest(ModelState);
            if (_taskRepository.CreateTask(new Models.Task
            {
                Id = taskToCreate.Id,
                Name = taskToCreate.Name,
                MaxMark = taskToCreate.MaxMark,
                Description = taskToCreate.Description,
                Priority = taskToCreate.Priority,
                Created = taskToCreate.Created,
                StartAccept = taskToCreate.StartAccept,
                CloseAccept = taskToCreate.CloseAccept
            }))
                return Ok();
            else
                return BadRequest();
        }
        [HttpPut]
        public ActionResult UpdateStudent([FromBody] TaskDto taskToUpdate)
        {
            if (taskToUpdate == null)
                return BadRequest();
            if (_taskRepository.UpdateTask(new Models.Task
            {
                Id = taskToUpdate.Id,
                Name = taskToUpdate.Name,
                Description = taskToUpdate.Description,
                MaxMark= taskToUpdate.MaxMark,
                Priority= taskToUpdate.Priority,
                Created= taskToUpdate.Created,
                StartAccept = taskToUpdate.StartAccept,
                CloseAccept = taskToUpdate.CloseAccept
            }))
                return Ok();
            else
                return BadRequest();
        }

        [HttpDelete("{id}")]
        public ActionResult DeleteTask(int id)
        {
            if (_taskRepository.DeleteTask(id))
                return Ok();
            else
                return BadRequest();
        }
    }
}
