﻿using Microsoft.AspNetCore.Mvc;
using TeacherJournal.Dto;
using TeacherJournal.Models;
using TeacherJournal.Interfaces;

namespace TeacherJournal.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentTaskController : Controller
    {
        private IStudentTaskRepository _studentTaskRepository;
        public StudentTaskController(IStudentTaskRepository studentTaskRepository)
        {
            _studentTaskRepository = studentTaskRepository;
        }
        [HttpGet]
        public ActionResult<IEnumerable<StudentTaskDto>> GetStudentTasks()
        {
            return _studentTaskRepository.GetStudentTasks().Select(s => new StudentTaskDto(s)).ToList();
        }

        [HttpGet]
        [Route("all")]
        public ActionResult<IEnumerable<StudentWithTasksDto>> GetStudentTasksAll()
        {
            return _studentTaskRepository.GetAllTasksForStudents().ToList();
        }

        [HttpGet("{studentId}")]
        public ActionResult<StudentWithTasksDto> GetStudentTasksPerStudent(int studentId)
        {
            return _studentTaskRepository.GetTasksForStudent(studentId);
        }

        [HttpPut]
        public ActionResult UpdateTaskForStudent(StudentTaskDto st)
        {
            if (_studentTaskRepository.UpdateStudentTask(new Models.StudentTask
            {
                StudentId = st.StudentId,
                TaskId = st.TaskId,
                Finished = st.Finished,
                Mark = st.Mark
            }))
                return Ok();
            return BadRequest();
        }

        [HttpDelete]
        public ActionResult DeteteStudentTask(int studentId, int taskId)
        {
            if (_studentTaskRepository.DeleteStudentTask(studentId, taskId)){
                return Ok();
            }
            return BadRequest();
        }
    }
}
