﻿using TeacherJournal.Models;
namespace TeacherJournal.Interfaces
{
    public interface ITaskRepository
    {
        ICollection<Models.Task> GetTasks();
        Models.Task GetTask(int id);
        bool CreateTask(Models.Task task);
        bool UpdateTask(Models.Task task);
        bool DeleteTask(int id);
    }
}
