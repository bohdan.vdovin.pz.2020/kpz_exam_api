﻿using TeacherJournal.Models;
using TeacherJournal.Dto;
namespace TeacherJournal.Interfaces
{
    public interface IStudentTaskRepository
    {
        ICollection<StudentTask> GetStudentTasks();
        StudentWithTasksDto GetTasksForStudent(int studentId);
        bool CreateStudentTask(StudentTask st);
        bool DeleteStudentTask(int studentId, int taskId);
        bool UpdateStudentTask(StudentTask st);
        ICollection<StudentWithTasksDto> GetAllTasksForStudents();
    }
}