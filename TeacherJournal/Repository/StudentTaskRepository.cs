﻿using TeacherJournal.Interfaces;
using TeacherJournal.Models;
using TeacherJournal.Dto;

namespace TeacherJournal.Repository
{
    public class StudentTaskRepository : IStudentTaskRepository
    {
        private JournalContext _dbContext;
        public StudentTaskRepository(JournalContext journalContext)
        {
            _dbContext = journalContext;
        }
        public bool CreateStudentTask(StudentTask st)
        {
            _dbContext.StudentTasks.Add(st);
            return Save();
        }

        public bool DeleteStudentTask(int studentId, int taskId)
        {
            var st = _dbContext.StudentTasks.Where(x => x.StudentId == studentId && x.TaskId == taskId).FirstOrDefault();
            if (st != null)
            {
                _dbContext.Remove(st);
                return Save();
            }
            else
                return false;
        }

        public ICollection<StudentWithTasksDto> GetAllTasksForStudents()
        {
            var groups = _dbContext.StudentTasks.GroupBy(s => s.Student).ToList();
            return groups.Select(g => new StudentWithTasksDto
            {
                StudentId = g.Key.Id,
                FirstName = g.Key.FirstName,
                LastName = g.Key.LastName,
                Marks = g.Select(st => new StudentTaskDto
                {
                    StudentId = st.StudentId,
                    TaskId = st.TaskId,
                    Mark = st.Mark,
                    Finished = st.Finished
                }).ToList()
            }).ToList();
        }

        public ICollection<StudentTask> GetStudentTasks()
        {
            return _dbContext.StudentTasks.ToList();
        }

        public StudentWithTasksDto GetTasksForStudent(int studentId)
        {
            var student = _dbContext.Students.FirstOrDefault(s => s.Id == studentId);
            if (student == null)
                return null;
            var marks = _dbContext.StudentTasks.Where(st => st.StudentId == studentId)
                .Select(m => new StudentTaskDto {
                    StudentId = m.StudentId,
                    TaskId = m.TaskId,
                    Finished = m.Finished,
                    Mark = m.Mark
                }).ToList();
            return new StudentWithTasksDto
            {
                StudentId = student.Id,
                FirstName = student.FirstName,
                LastName = student.LastName,
                Marks = marks
            };
        }

        public bool UpdateStudentTask(StudentTask st)
        {
            if (!_dbContext.StudentTasks.Any(x => x.TaskId == st.TaskId && x.StudentId == st.StudentId))
            {
                return CreateStudentTask(st);
            }
            _dbContext.Update(st);
            return Save();
        }
        protected bool Save()
        {
            var saved = _dbContext.SaveChanges();
            return saved > 0 ? true : false;
        }
    }
}
