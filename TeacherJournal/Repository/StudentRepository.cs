﻿using TeacherJournal.Interfaces;
using TeacherJournal.Models;

namespace TeacherJournal.Repository
{
    public class StudentRepository : IStudentRepository
    {
        private JournalContext _dbContext;
        public StudentRepository(JournalContext journalContext)
        {
            _dbContext = journalContext;
        }
        public bool CreateStudent(Student student)
        {
            _dbContext.Students.Add(student);
            return Save();
        }

        public bool DeleteStudent(int id)
        {
            var student = _dbContext.Students.Where(s => s.Id == id).FirstOrDefault();
            if (student != null)
            {
                _dbContext.Remove(student);
                return Save();
            }
            return false;
        }

        public Student GetStudent(int id)
        {
            return _dbContext.Students.Where(s => s.Id == id).FirstOrDefault();
        }

        public Student GetStudent(string firstname, string lastname)
        {
            return _dbContext.Students.Where(s =>
                s.FirstName == firstname && s.LastName == lastname)
                .FirstOrDefault();
        }

        public ICollection<Student> GetStudents()
        {
            return _dbContext.Students.ToList();
        }

        public bool UpdateStudent(Student student)
        {
            if (!_dbContext.Students.Any(x => x.Id == student.Id))
            {
                return false;
            }
            _dbContext.Update(student);
            return Save();

        }
        protected bool Save()
        {
            var saved = _dbContext.SaveChanges();
            return saved > 0 ? true: false;
        }
    }
}
