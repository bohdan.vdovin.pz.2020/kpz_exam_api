﻿using TeacherJournal.Interfaces;
using TeacherJournal.Models;
namespace TeacherJournal.Repository
{
    public class TaskRepository : ITaskRepository
    {
        private JournalContext _dbContext;
        public TaskRepository(JournalContext journalContext)
        {
            _dbContext = journalContext;
        }

        public bool CreateTask(Models.Task task)
        {
            _dbContext.Tasks.Add(task);
            return Save();
        }

        public bool DeleteTask(int id)
        {
            var task = _dbContext.Tasks.FirstOrDefault(t => t.Id == id);
            if(task != null)
            {
                _dbContext.Remove(task);
                return Save();
            }
            return false;
        }

        public Models.Task GetTask(int id)
        {
            return _dbContext.Tasks.Where(t => t.Id == id).FirstOrDefault();
        }

        public ICollection<Models.Task> GetTasks()
        {
            return _dbContext.Tasks.ToList();
        }

        public bool UpdateTask(Models.Task task)
        {
            if (!_dbContext.Tasks.Any(x => x.Id == task.Id))
            {
                return false;
            }
            _dbContext.Update(task);
            return Save();
        }
        protected bool Save()
        {
            var saved = _dbContext.SaveChanges();
            return saved > 0 ? true : false;
        }
    }
}
