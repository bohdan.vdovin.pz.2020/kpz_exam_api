﻿namespace TeacherJournal.Models
{
    public class StudentTask
    {
        public int TaskId { get; set; }
        public Models.Task Task { get; set; }
        public int StudentId { get; set; }
        public Student Student { get; set; }
        public float Mark { get; set; }
        public DateTime Finished { get; set; }
    }
}
