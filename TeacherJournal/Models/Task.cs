﻿namespace TeacherJournal.Models
{
    public class Task
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public float MaxMark { get; set; }
        public string Description { get; set; }
        public string Priority { get; set; }
        public DateTime Created { get; set; }
        public DateTime StartAccept { get; set; }
        public DateTime CloseAccept { get; set; }
        public ICollection<StudentTask> StudentTasks { get; set; } 
    }
}
