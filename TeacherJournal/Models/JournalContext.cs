﻿using Microsoft.EntityFrameworkCore;

namespace TeacherJournal.Models
{
    public class JournalContext : DbContext 
    {
        public JournalContext(DbContextOptions<JournalContext> options) : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<StudentTask>()
                .HasKey(st => new { st.StudentId, st.TaskId });
            modelBuilder.Entity<StudentTask>()
                .HasOne(s => s.Student)
                .WithMany(t => t.StudentTasks)
                .HasForeignKey(t => t.StudentId);
            modelBuilder.Entity<StudentTask>()
                .HasOne(t => t.Task)
                .WithMany(t => t.StudentTasks)
                .HasForeignKey(t => t.TaskId);

            Task ts1 = new Task
            {
                Id = 1,
                Name = "Lab1",
                Description = "Introduciton Lab",
                MaxMark = 5,
                Priority = "low",
                Created = DateTime.Parse("2020-03-10 20:49:06"),
                StartAccept = DateTime.Parse("2021-09-1 00:00:00"),
                CloseAccept = DateTime.Parse("2021-09-8 00:00:00")
            };
            Task exam = new Task
            {
                Id = 2,
                Name = "Exam",
                Description = "Write the program to huck vns",
                MaxMark = 50,
                Priority = "Hight",
                Created = DateTime.Parse("2021-03-10 20:49:06"),
                StartAccept = DateTime.Parse("2021-12-1 8:00:00"),
                CloseAccept = DateTime.Parse("2021-12-1 20:00:00")
            };
            Student st1 = new Student
            {
                Id = 1,
                FirstName = "Tomas",
                LastName = "Shelbi",
                Email = "tomas.shelbi@gmail.com",
                Age = 25,
            };
            Student st2 = new Student
            {
                Id = 2,
                FirstName = "Jonatan",
                LastName = "Joster",
                Email = "jostar111@gmail.com",
                Age = 16,
            };
            modelBuilder.Entity<Student>()
                .HasData(st1, st2);
            modelBuilder.Entity<Task>().HasData(ts1, exam);
            modelBuilder.Entity<StudentTask>().HasData(new StudentTask
            {
                StudentId = st1.Id,
                TaskId = ts1.Id,
                Mark = 4,
                Finished = DateTime.Parse("2021-08-1 12:23:00")
            });
        }
        public DbSet<Student> Students { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<StudentTask> StudentTasks { get; set; }
    }
}
